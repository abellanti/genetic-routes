import java.util.ArrayList;


//data structure to parse data from the given txt files (ex.: A1.txt)
//must be loaded with DataReader
public class CVRP_DS
{

    private int customers_number;
    private int vehicles_number;
    private int capacity_per_vechicle;
    private Node depot;
    private ArrayList<Node> customers;
    public ArrayList<Node> linehauls;
    public ArrayList<Node> backhauls;

    public CVRP_DS(int customers_number, int vehicles_number, Node depot)
    {
        this.customers_number=customers_number;
        this.vehicles_number=vehicles_number;
        this.depot=depot;
        this.customers=new ArrayList<>();
        linehauls = new ArrayList<>();
        backhauls = new ArrayList<>();
    }


    public int getCustomers_number() {
        return customers_number;
    }

    public void setCustomers_number(int customers_number) {
        this.customers_number = customers_number;
    }

    public void addCustomer(Node n)
    {
        this.customers.add(n);
    }

    public int getVehicles_number() {
        return vehicles_number;
    }

    public void setVehicles_number(int vehicles_number) {
        this.vehicles_number = vehicles_number;
    }

    public Node getDepot() {
        return depot;
    }

    public void setDepot(Node depot) {
        this.depot = depot;
    }

    public ArrayList<Node> getCustomers() {
        return customers;
    }

    public int getCapacity_per_vechicle() {
        return capacity_per_vechicle;
    }

    public void setCapacity_per_vechicle(int capacity_per_vechicle) {
        this.capacity_per_vechicle = capacity_per_vechicle;
    }
}
