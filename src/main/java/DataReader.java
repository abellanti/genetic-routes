/*Generic parser that reads the data from the given txt and put them in a data structure
*
*
* args[0] .txt path*/


import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class DataReader {

    public CVRP_DS read(String file, short verbose)
    {
        //try the txt reading
        Scanner txtScanner=null;
        try {
            txtScanner = new Scanner(new File(file));
        } catch (FileNotFoundException e) {
            System.out.println("Couldn't read the txt file, exit.");
            return null;
        }

        Node emptyDepot = null;
        CVRP_DS datas = new CVRP_DS(0,0,emptyDepot);
        int i=0;
        int nodeNumber=1;
        //very stupid reading, I get what I'm reading via line index
        while(txtScanner.hasNextLine())
        {
            String currentLine = txtScanner.nextLine();
            i++;
            if(i<4)
            {
                switch(i)
                {
                    //customer number
                    case 1:
                        datas.setCustomers_number(Integer.parseInt(currentLine));
                        break;
                    //vechicle number
                    case 3:
                        datas.setVehicles_number(Integer.parseInt(currentLine));
                        break;
                    default:
                        break;
                }
            }
            //initializing the depot
            else if(i==4)
            {
                datas.setDepot(new Node(Integer.parseInt(currentLine.split("   ")[0]) //depot X
                                       ,Integer.parseInt(currentLine.split("   ")[1]) //depot Y
                                       ,0, 0,0)); //no delivery or pickup since it's a depot, default id=0
                //capacity of each vehicle
                datas.setCapacity_per_vechicle(Integer.parseInt(currentLine.split("   ")[3]));
            }
            //adding the customers
            else
            {
                Node n = new Node(Integer.parseInt(currentLine.split("   ")[0]) //x
                                 ,Integer.parseInt(currentLine.split("   ")[1]) //y
                                 ,Integer.parseInt(currentLine.split("   ")[2]) //delivery
                                 ,Integer.parseInt(currentLine.split("   ")[3]) //pickup
                                 ,nodeNumber); //id

                datas.addCustomer(n);
                nodeNumber++;
                if(n.getDelivery()>0)
                    datas.linehauls.add(n);
                else
                    datas.backhauls.add(n);

            }
        }

        if(verbose==1)
        {
            System.out.println("Finished loading: visualize the content? (Y=yes)");
            Scanner inputScanner = new Scanner(System.in);
            String choice = inputScanner.nextLine();
            if(choice.equals("y") || choice.equals("Y"))
            {
                System.out.println("DATA: ");
                System.out.println("\tCustomers number: "+datas.getCustomers_number());
                System.out.println("\tVehicles number: "+datas.getVehicles_number());
                System.out.println("\tCapacity per vehicle: "+datas.getCapacity_per_vechicle());
                System.out.println("\n\tDepot: X:"+datas.getDepot().getX()+"\n\tY:"+datas.getDepot().getY());
                i=0;
                for(Node n: datas.getCustomers())
                {
                    i++;
                    System.out.println("\n\tCustomer "+i+": \n\tX:"+n.getX()+"\n\tY:"+n.getY()+"\n\tDelivery amount: "+n.getDelivery()+"\n\tPickup amount: "+n.getPickup());

                }
                System.out.println("\n\tLinehauls: ");
                for(Node n: datas.linehauls)
                    System.out.println(n.getId());
                System.out.println("\n\tBackhauls: ");
                for(Node n: datas.backhauls)
                    System.out.println(n.getId());
                System.out.println("Any key to continue)");
                inputScanner.nextLine();

            }
        }

        return datas;
    }
}
