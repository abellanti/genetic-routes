import java.util.ArrayList;
import java.util.Comparator;
import java.util.concurrent.Callable;
import java.util.concurrent.ThreadLocalRandom;


/*
* This class generates an initial pool of randomic solutions for the CVRP, by following these costraints:
*  1) Respect the number of vehicle of the instance
   2) Backhaul only routes aren't allowed
   3) Each route starts from the depot, serves the linehaul first and then the backhauls
   4) Linehaul only routes are allowed
   5) Can't exceed the maximum capacity of each vehicle
*
* * pls note that:
 *        - linehaul = delivery
 *        - backhaul = pickup*/

public class SolutionGenerator implements Callable<ArrayList<Solution>>{

    private int numberToGenerate;
    private CVRP_DS datas;
    ArrayList<Node> linehauls;
    ArrayList<Node> backhauls;

    public SolutionGenerator(int numberToGenerate, CVRP_DS datas){
        this.setNumberToGenerate(numberToGenerate);
        this.datas=datas;

    }

    public Solution generate(){
        linehauls = new ArrayList<>(datas.linehauls);
        backhauls = new ArrayList<>(datas.backhauls);
        int veiclesNumber= datas.getVehicles_number();
        int weightMax=datas.getCapacity_per_vechicle();
        ArrayList<Route> routes = new ArrayList<>();
        Integer vehicleWeight[] = new Integer[veiclesNumber];
        int cont=0;
        int index;
        boolean failed=false;

        for(int i=0; i<veiclesNumber; i++){ //I assume that every vehicle starts full
            vehicleWeight[i]=weightMax;
        }


        //I can't generate a route with only backhouls and I must generate a route for every vehicle
        if(linehauls.size()<veiclesNumber){
            return null;
        }

        //initializing one route per vehicle, each of one starts from the given depot

        for(int i=0; i<veiclesNumber; i++)
        {
            routes.add(new Route(datas.getDepot()));
        }


        while (linehauls.size()>0){ //While I've linehauls to serve
            for(int i=0; i<veiclesNumber; i++){ //Every vehicle
                if(linehauls.size()>0){
                    index = ThreadLocalRandom.current().nextInt(linehauls.size());
                    if(vehicleWeight[i]>=linehauls.get(index).getDelivery()){ //if I can serve that node
                        cont=0;
                        routes.get(i).appendNode(linehauls.get(index)); //add the node to the route of the vehicle
                        vehicleWeight[i]-=linehauls.get(index).getDelivery(); //remove the goods from the vehicle
                        linehauls.remove(index);
                    }
                    else{
                        cont++;
                        if(cont>=1000){
                            failed=true;
                            break;
                        }
                    }
                }
                else{
                    break;
                }
            }
            if(failed){
                return null;
            }
        }

        cont=0;

        for(int i=0; i<veiclesNumber; i++){ //I assume that every vehicle was loaded with just the necessary goodies
            vehicleWeight[i]=0;
        }

        while(backhauls.size()>0){ //While I've backhauls to serve
            for(int i=0; i<veiclesNumber; i++){ //Every vehicle
                if(backhauls.size()>0){
                    index=ThreadLocalRandom.current().nextInt(backhauls.size()); //Serves a random backhaul
                    if((vehicleWeight[i]+backhauls.get(index).getPickup())<=weightMax){ //If I can serve that node
                        cont=0;
                        routes.get(i).appendNode(backhauls.get(index)); //Add the node to the route of that vehicle
                        vehicleWeight[i]+=backhauls.get(index).getPickup(); //Add the weight of the goodies retired from the node
                        backhauls.remove(index); //Remove the node of the backhauls to serve
                    }
                    else{
                        cont++;
                        if(cont >=1000){
                            failed=true;
                            break;
                        }
                    }
                }
                else{
                    break;
                }
            }
            if(failed){
                return null;
            }
        }

        for(Route route : routes){ //every vehicle must return to the depot
            route.appendNode(datas.getDepot());
        }

        if(linehauls.size()>0 || backhauls.size()>0)
            return null;

        return new Solution(routes);
    }


    @Override
    public ArrayList<Solution> call()
    {
        ArrayList<Solution> toReturn = new ArrayList<>();
        for(int i=0; i<numberToGenerate; i++) {
            Solution s = generate();
            if(s!=null)
                toReturn.add(s);
        }
        toReturn.sort(Comparator.naturalOrder());
        System.gc();

        return toReturn;
    }

    public void setNumberToGenerate(int numberToGenerate) {
        this.numberToGenerate = numberToGenerate;
    }
}
