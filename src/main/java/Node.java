public class Node implements Comparable<Node>
{

    private int id;
    private int x;
    private int y;
    private int delivery;
    private int pickup;

    public Node()
    {
        this.x=0;
        this.y=0;
        this.delivery=0;
        this.pickup=0;
        this.id=0;
    }

    public Node(int x, int y, int delivery, int pickup,int id)
    {
        this.x=x;
        this.y=y;
        this.delivery=delivery;
        this.pickup=pickup;
        this.id=id;
    }

    public Node(Node n){
        this.x = n.x;
        this.y = n.y;
        this.delivery = n.delivery;
        this.pickup = n.pickup;
        this.id = n.id;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getDelivery() {
        return delivery;
    }

    public void setDelivery(int delivery) {
        this.delivery = delivery;
    }

    public int getPickup() {
        return pickup;
    }

    public void setPickup(int pickup) {
        this.pickup = pickup;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    //useful to avoid checking if a node is delivery or pickup directly into the code. Returns the !=0 field of the two
    public int getActualWork()
    {
        if(this.delivery!=0)
            return this.delivery;
        if(this.pickup!=0)
            return this.pickup;
        return 0;
    }

    public float getDistance(Node n){
        float result = 0.0f;
        result = (float) Math.sqrt((Math.pow(new Float(this.getX()-n.getX()),2) + Math.pow(new Float(this.getY()-n.getY()),2)));
        return result;
    }

    public short equals(Node n)
    {
        if(this.getId()==n.getId())
            return 1;
        else
            return 0;
    }

    public int compareTo(Node n)
    {
        return Integer.compare(getActualWork(),n.getActualWork());
    }
}
