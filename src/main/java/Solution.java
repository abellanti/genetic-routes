import java.io.Serializable;
import java.util.ArrayList;

public class Solution implements Comparable<Solution>, Serializable{
    private float cost;
    private ArrayList<Route> routes;

    public Solution(ArrayList<Route> routes){
        cost = 0.0f;
        this.routes = routes;
        calculateCost();
    }

    public float getCost() {
        return cost;
    }

    public void calculateCost(){
        cost = 0.0f;
        for(Route r : routes){
            r.calculateCost();
            cost +=r.getCost();
        }
    }

    public ArrayList<Route> getRoutes() {
        return routes;
    }

    public int compareTo(Solution s){
        return Double.compare(cost,s.getCost());
    }

    public static short isDuplicated(Solution s1, Solution s2)
    {
        for(int i=0; i<s1.getRoutes().size(); i++)
        {
            for(int j=0; j< s1.getRoutes().get(i).getNodes().size(); j++)
            {
                if(s1.getRoutes().get(i).getNodes().get(j).getId()!=s2.getRoutes().get(i).getNodes().get(j).getId())
                    return 0;
            }
        }

        return 1;
    }

    public void overrideRoute(int index, Route r)

    {
        this.routes.set(index,r);
        this.calculateCost();
    }
}
