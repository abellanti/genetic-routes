import java.io.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.stream.Collectors;

/*
* Main test class
* args[0] = data file (ex. A1.txt)
* args[1] = save file (to write a brief report of the results)
* args[2] = initial size of population
* args[3] = max number of iterations
* args[4] = initial tolerance (means that an individual is accepted if its optimum is worse than the current best one plus a certain tolerance)
* args[5] = tolerance reduction per iteration
* args[6] = individual mating chance %. In other words one individual can initially mate with only this % of the population, to prove
*           that it is fertile (a.k.a.: can produce children that are lower than the optimum or than the optimum+the tolerance).
*           For every child that beat the optimum (without the tolerance) this number is increased.
*          
* args[7] = population mating chance %. Although args[6] speeds the iteration, every single one individual has its chances to mate, and
*           if very large population are used this can lead to very slow iterations. Also this could lead to the forced choice
*           to use very small args[6] values. So args[7] can put a stop to this. The population itself has a chance to demonstrate
*           its fertility, in percentual. So this % of the total population can mate: every individual that get at least one
*           child that beats the best optimum (without tolerance considered) so far increases this percentual.
*args[8] = population size control. If it is true then the population is refreshed with the children, but it can't grow from it's initial dimension*
*args[9] = truncate population to this size in generation phase: this allows to generate a greater number of initial solutions but to use only a portion of them.
*           tipically an initial size that is way higher than the truncation performs better.
*
*args[10] = path for saving the best solution. So even if the algorithm is stopped it is possible to always retrieve the last best solution
*args[11] = at which size, during generation phase, a thread must sort and truncate it's results (to save ram). if 0 is disabled
*args[12] = same but for crossover (should be less than 1M, since crossover thread use single threaded sort). If 0 is disabled
*args[13] = population generation number per thread
*args[14] = multiplicative factor per pool (generation); 0 means disabled.
*args[15] = multiplicative factor per pool (crossover); 0 means disabled.
*args[16] = multicore boolean switch -> just for testing purpose
*args[17] = verbosity = if true the program prompt asking the user if he want to see the read data*/
public class MainExecutor
{

    //logging variables

    public static int usefulSchool = 0;
    public static int uselessSchool = 0;


    public static int belowTresholdOffsprings = 0;
    public static int overThresholdOffsprings = 0;
    public static int offspringsWithAsymmetricRoutes = 0;
    public static int offspringsThatDoesntServeAllTheNodes = 0;
    public static int offspringsWithBackOnly = 0;
    public static int tooHeavyOffsprings = 0;
    public static int offspringsWithTooShortRoutes = 0;
    public static int offspringsWithNotEnoughRoutes = 0;
    public static ArrayList<Integer> elitarianism = new ArrayList<>();
    public static ArrayList<Integer> splitBy = new ArrayList<>();
    public static ArrayList<Integer> splitPerc = new ArrayList<>();


    public void main(String args[])
    {
        //getting the number of threads that I can launch
        int nThread= Runtime.getRuntime().availableProcessors();

        //just transfering the args into variables
        String datasPath = args[0];
        String newPartialName = args[1]+"/"+datasPath.split("/")[datasPath.split("/").length-1].split("\\.")[0];
        String saveFilePath = args[1]+"/"+"temp";
        long initialPopulationSize = Long.parseLong(args[2]);
        int iterations = Integer.parseInt(args[3]);
        int tolerance = Integer.parseInt(args[4]);
        int reduction = Integer.parseInt(args[5]);
        float individualPerc = Float.parseFloat(args[6]);
        float populationPerc = Float.parseFloat(args[7]);
        float individualChance = 0; //will reflect the given %
        float populationChance = 0; //will reflect the given %
        short sizeControl = Short.parseShort(args[8]);
        int populationSizeTruncation = Integer.parseInt(args[9]);
        String bestSolutionPath = args[10]+"/bestbackup.txt";
        int generationRamSaver=  Integer.parseInt(args[11]);
        int crossoverRamSaver=  Integer.parseInt(args[12]);
        int generationsPerthread=Integer.parseInt(args[13]);
        int generationPoolFactor = Integer.parseInt(args[14]);
        int crossoverPoolFactor = Integer.parseInt(args[15]);
        short multiThreading = Short.parseShort(args[16]);
        short verboseReading = Short.parseShort(args[17]);
        int earlyStop = Integer.parseInt(args[20]);
        for(String s : args[21].split(" "))
            elitarianism.add(Integer.parseInt(s));

        for(String s : args[18].split(" "))
            splitBy.add(Integer.parseInt(s));

        for(String s : args[19].split(" "))
            splitPerc.add(Integer.parseInt(s));


        Scanner inputScan = new Scanner(System.in);

        //getting the datas from the txt
        DataReader ds = new DataReader();
        CVRP_DS datas = ds.read(datasPath,verboseReading);

        long startTime,stopTime;

        //this will host the Threads that will generate the initial population
        CopyOnWriteArrayList<SolutionGenerator> populationGenerators = new CopyOnWriteArrayList<>();

        //the population container
        ArrayList<Solution> solutions = new ArrayList<>();

        //various error cases
        if (datas == null)
        {
            System.out.println("Couldn't read the input file (check the path)");
            return;
        }

        PrintWriter saveFile;
        try
        {
            saveFile = new PrintWriter(new File(saveFilePath));
        } catch (FileNotFoundException e)
        {
            System.out.println("Couldn't find or create the savefile");
            return;
        }

        ExecutorService executor = Executors.newFixedThreadPool(nThread/2);

        System.out.println("Generating population");
        if(multiThreading==1)
        {
            int generated = 0;
            float genOptimum = 0;
            ArrayList<Future<ArrayList<Solution>>> list = new ArrayList<>();
            String prev = "";
            long target = initialPopulationSize;
            if(populationSizeTruncation>0)
                target = populationSizeTruncation;
            while(solutions.size()<target)
            {
                for(int i=0; i<(nThread/2)*generationPoolFactor; i++)
                    list.add(executor.submit(new SolutionGenerator(generationsPerthread,datas)));

                for(Future<ArrayList<Solution>> f: list)
                {
                    try
                    {
                        solutions.addAll(f.get());
                        generated+=f.get().size();
                        f.cancel(true);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } catch (ExecutionException e) {
                        e.printStackTrace();
                    }

                    //this saves ram: if the population size goes over the treshold then it is re-sorted and truncated
                    if(generationRamSaver!=0 && solutions.size() > generationRamSaver)
                    {
                        solutions = new ArrayList<>(solutions.parallelStream()
                                .sorted()
                                .collect(Collectors.toList()).subList(0,generationRamSaver));
                        System.gc();

                        genOptimum = solutions.get(0).getCost();

                    }
                }

                list.clear();
                System.gc();
                int exponent  = String.valueOf(generated).split("").length-1;
                int number = Integer.parseInt(String.valueOf(generated).split("")[0]);
                String toPrint = number+"*10^"+exponent+" generated, optimum: "+genOptimum+" / population size: "+solutions.size();
                if(!toPrint.equals(prev))
                    System.out.println(toPrint);
                prev = toPrint;

            }
        }
        else
        {
            SolutionGenerator populationGenerator = new SolutionGenerator(generationsPerthread, datas);
            int generated=0;
            float genOptimum = 0;
            Solution aux = null;
            while(generated<initialPopulationSize)
            {
                aux = populationGenerator.generate();
                if(aux!=null)
                {
                    solutions.add(aux);
                    generated++;
                }

                if(generationRamSaver!=0 && solutions.size()>generationRamSaver)
                {
                    solutions.sort(Comparator.naturalOrder());
                    solutions = new ArrayList<>(solutions.subList(0,generationRamSaver));
                    genOptimum = solutions.get(0).getCost();

                }
                System.out.println(generated+" generated, optimum: "+genOptimum);
            }
        }

        //truncation even if we generated more individuals: allows to exploit better optimums with small populations
        if(sizeControl==1 && solutions.size()>populationSizeTruncation)
        {
            solutions = new ArrayList<>(solutions.parallelStream()
                    .sorted()
                    .collect(Collectors.toList()).subList(0,populationSizeTruncation));
        }

        if(elitarianism.get(0)==0)
            solutions = eliteRemover(solutions, splitBy.get(0), splitPerc.get(0));

        if(verboseReading==1)
        {
            System.out.println("Population created: wanna see a random solution? (y if yes)");
            String input = inputScan.nextLine();
            if(input.equals("y"))
            {
                int random = ThreadLocalRandom.current().nextInt(10000);
                for(Route r: solutions.get(random).getRoutes())
                {
                    for(Node n: r.getNodes())
                        System.out.print("\t-->"+n.getId());
                    System.out.println();
                    int deliverying = 0;
                    int pickupping = 0;
                    for(Node n: r.getNodes())
                    {
                        if(n.getDelivery()>0 && n.getId()!=0)
                        {
                            System.out.print("\t-->LIN");
                            deliverying+=n.getDelivery();
                        }
                        else if(n.getId()!=0)
                        {
                            System.out.print("\t-->BACK");
                            pickupping+=n.getPickup();
                        }
                        else
                            System.out.print("\t-->DEPOT");
                    }
                    System.out.println();
                    System.out.println("Total delivery: "+deliverying+"<"+datas.getCapacity_per_vechicle()+" / Total pickup: "+pickupping+"<"+datas.getCapacity_per_vechicle());
                    System.out.println();
                    System.out.println();
                }
                inputScan.nextLine();
            }
        }

        if(sizeControl==1 && solutions.size()>populationSizeTruncation)
        {
            solutions = new ArrayList<>(solutions.parallelStream()
                    .sorted()
                    .collect(Collectors.toList()).subList(0,populationSizeTruncation));
        }


        int backupInitialSize = (int)initialPopulationSize;

        //setting the dimension of the population, according to the eventual size control and truncating
        if(sizeControl==1 && solutions.size()>populationSizeTruncation)
        {
            initialPopulationSize=populationSizeTruncation;
            solutions = new ArrayList<>(solutions.subList(0,populationSizeTruncation));
        }


        //getting the initial best solution
        float optimum = solutions.get(0).getCost();
        Solution bestSolution = solutions.get(0);
        float initialOptimum = solutions.get(0).getCost();
        float previousOptimum = optimum;

        //log of the genetic setup
        saveFile.println("LOG");
        saveFile.println("Mating on "+datasPath);
        saveFile.println("PARAMETERS:"+
        "\n\tPopulation size (initial): "+backupInitialSize+
        "\n\tTruncated to: "+populationSizeTruncation+
        "\n\t"+iterations+" iterations"+
        "\n\tTolerance: "+tolerance+", decreasing of "+reduction+" per iteration"+
        "\n\tIndividual mating chance: "+individualPerc+
        "\n\tPopulation mating chance: "+populationPerc+
        "\n\tSize control = "+sizeControl+
        "\n\tMain Thread sort every "+generationRamSaver+" elements"+
        "\n\tEvery generation thread generates "+generationsPerthread+" solutions per run"+
        "\n\tGeneration pool does "+generationPoolFactor+" jobs per cycle"+
        "\n\tCrossover pool does "+crossoverPoolFactor+" jobs per cycle"+
        "\n\tMultithread = "+multiThreading+
        "\n\tSplit population by = "+args[18]+
        "\n\tTake % from every sector = "+args[19]+
        "\n\tElitarianism pattern (generation, iteration, crossover) = "+args[21]);


        saveFile.close();


        ArrayList<Solution> definitiveChildren = new ArrayList<>(); //contains the offsprings


        //doing the iterations
        for (int i = 0; i < iterations; i++)
        {
            usefulSchool =0;
            uselessSchool = 0;
            belowTresholdOffsprings = 0;
            overThresholdOffsprings = 0;
            offspringsWithAsymmetricRoutes = 0;
            offspringsThatDoesntServeAllTheNodes = 0;
            offspringsWithBackOnly = 0;
            tooHeavyOffsprings = 0;
            offspringsWithTooShortRoutes = 0;
            offspringsWithNotEnoughRoutes = 0;

            Map<Float, Solution> map = new LinkedHashMap<>();
            for (Solution s : solutions) {
                map.put(Float.parseFloat(new String(String.valueOf((int)s.getCost()))), s);
            }
            int shift = solutions.size()-map.size();
            if(shift<0)
                shift=0;
            solutions.clear();
            solutions.addAll(map.values());
            map.clear();

            individualPerc = Float.parseFloat(args[6]);
            populationPerc = Float.parseFloat(args[7]);
            populationChance = solutions.size()*populationPerc/100;
            individualChance = solutions.size()*individualPerc/100;

            startTime=System.currentTimeMillis();
            int currentPopulationSize = solutions.size();
            int realJ = 0;

            definitiveChildren.clear();


            float newOptimum = optimum; //at the beginning of the iteration we match the initial and best optimum

            //mating the j-th individual of the population
            for (int j = 0; realJ < populationChance && solutions.size()>2; j++)
            {
                individualPerc = Float.parseFloat(args[6]); //the mating chance for the individual resets for every individual, while the population one's persists

                //mating the selected % of individual with the j-th one
                for (int k = j+1; (k < (k+(int)(individualChance)) && k<solutions.size()) ; k++)
                {
                    if(j==k)
                        continue;

                    //setting the mating chances
                    populationChance = solutions.size()*populationPerc/100;
                    individualChance = solutions.size()*individualPerc/100;

                    //this is technically impossible, but who knows maybe you'll get a very nice population!
                    if(individualChance>solutions.size())
                        individualChance=solutions.size();

                    if(populationChance>solutions.size())
                        populationChance=solutions.size();


                    //on-screen log
                    int target = 0;
                    if(((k+(int)(individualChance)-1)) < solutions.size()-1)
                        target = ((k+(int)(individualChance)-1));
                    else
                        target = solutions.size()-1;


                    System.out.println("------------------------------------------------------------------");
                    System.out.println("Iteration "+(i+1)+"/"+iterations+".\n" +
                                       "Population of "+currentPopulationSize+". \n" +
                                       "Mating ->"+realJ+" with "+k+"-"+target+" on "+newPartialName+"\n" +
                                       "STATS: \n\t" +
                                              "Initial optimum (from random solutions): "+initialOptimum+
                                              "\n\tCurrent optimum = "+optimum+" -> with tolerance "+(optimum*tolerance)/100+"" +
                                              "\n\tBest optimum so far: "+newOptimum+
                                              "\n\n\t Children below/in threshold: "+ belowTresholdOffsprings +
                                              "\n\n\t Children over threshold: "+ overThresholdOffsprings +
                                              "\n\n\t children size: "+definitiveChildren.size()+
                                              "\n\n\t current population size: "+solutions.size()+
                                              "\n\n\t removed duplicates: "+shift+
                                              "\n\n\tremaining chances to be fertile for this individual: ~"+individualPerc+"% ("+(int)individualChance+")"+
                                              "\n\tremaining chances to be fertile for this population: ~"+populationPerc+"% ("+(int)populationChance+")" +
                                              "\n\tuseful school: "+ usefulSchool +
                                              "\n\tuseless school: "+ uselessSchool +
                                              "\n\tOffsprings aborted for asymmetric routes: " +offspringsWithAsymmetricRoutes+
                                              "\n\tOffsprings with not all nodes served: "+offspringsThatDoesntServeAllTheNodes+
                                              "\n\tOffsprings with BACK only routes: "+offspringsWithBackOnly+
                                              "\n\tOffsprings that are too heavy: "+tooHeavyOffsprings+
                                              "\n\tOffsprings that have too short routes: "+offspringsWithTooShortRoutes+
                                              "\n\tOffsprings with number of Routes < number of vehicels: "+offspringsWithNotEnoughRoutes
                                              );
                    System.out.println("------------------------------------------------------------------");
                    try {
                        saveFile = new PrintWriter(new File(bestSolutionPath));
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }


                    //and saving on file the current solution
                    saveFile.println("Best solution in this iteration: "+bestSolution.getCost());
                    saveFile.println("\n");
                    for(Route r: bestSolution.getRoutes())
                    {
                        int deliverying = 0;
                        int pickupping = 0;
                        for (Node n : r.getNodes()) {
                            saveFile.append("\t-->" + n.getId());
                            if (n.getDelivery() > 0 && n.getId() != 0) {
                                saveFile.append("\t-->LIN");
                                deliverying += n.getDelivery();
                            } else if (n.getId() != 0) {
                                saveFile.append("\t-->BACK");
                                pickupping += n.getPickup();
                            } else
                                saveFile.append("\t-->DEPOT");

                            saveFile.append("\n");
                        }
                        saveFile.append("");
                        saveFile.append("\n");
                        saveFile.append("Total delivery: " + deliverying + "<" + datas.getCapacity_per_vechicle() + " / Total pickup: " + pickupping + "<" + datas.getCapacity_per_vechicle());
                        saveFile.append("\n");
                    }
                    saveFile.close();

                    if(multiThreading==1)
                    {
                        ArrayList<Future<ArrayList<Solution>>> list = new ArrayList<>();
                        for(int m = 0; m<(nThread/2)*crossoverPoolFactor; m++)
                        {
                            if(((j+1)+individualChance-1)<(solutions.size()-1))
                                list.add(executor.submit(new Crossover(solutions.get(j).getRoutes(),(solutions.subList((j+1),(int)(k+individualChance))),datas.getCapacity_per_vechicle(),optimum,tolerance,crossoverRamSaver)));
                            else
                                list.add(executor.submit(new Crossover(solutions.get(j).getRoutes(),(solutions.subList((j+1),solutions.size())),datas.getCapacity_per_vechicle(),optimum,tolerance,crossoverRamSaver)));
                            j++;
                        }

                        for(Future<ArrayList<Solution>> f: list)
                        {
                            try {
                                if(f.get()!=null)
                                {
                                    definitiveChildren.addAll(f.get());
                                    f.cancel(true);
                                }
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            } catch (ExecutionException e) {
                                e.printStackTrace();
                            }

                            if(crossoverRamSaver>0 && definitiveChildren.size()>crossoverRamSaver)
                            {
                                definitiveChildren = new ArrayList<>(definitiveChildren.parallelStream()
                                        .sorted()
                                        .collect(Collectors.toList()).subList(0,crossoverRamSaver));
                                System.gc();

                            }

                        }
                        k=solutions.size();
                    }
                    else
                    {
                        Crossover crossoverGen = new Crossover(solutions.get(j).getRoutes(),null,datas.getCapacity_per_vechicle(),optimum,tolerance,crossoverRamSaver);
                        int limit = 0;
                        if(k+individualChance<solutions.size())
                            limit=k+((int)individualChance);
                        else
                            limit = solutions.size()-k;

                        for(int m=k; m<limit; m++)
                        {
                            ArrayList<ArrayList<Route>> offsprings = new ArrayList<>();

                            offsprings.add(crossoverGen.likeMonkeys(solutions.get(j).getRoutes(),solutions.get(m).getRoutes()));
                            offsprings.add(crossoverGen.likeMonkeys(solutions.get(m).getRoutes(),solutions.get(j).getRoutes()));

                            ArrayList<Route> best = solutions.get(j).getRoutes();

                            short changed = 0;
                            for(ArrayList<Route> element: offsprings)
                            {
                                if(element!=null && new Solution(element).getCost()<new Solution(best).getCost())
                                    best = element;
                                changed = 1;
                            }

                            if(changed==1)
                                definitiveChildren.add(new Solution(best));
                        }
                        k=solutions.size();
                    }

                    if(sizeControl==1 && definitiveChildren.size()>populationSizeTruncation)
                    {
                        definitiveChildren = new ArrayList<>(definitiveChildren.parallelStream()
                                .sorted()
                                .collect(Collectors.toList()).subList(0,populationSizeTruncation));
                        System.gc();
                    }
                    //exploring all of the obtained offsprings
                    for(Solution candidate: definitiveChildren)
                    {
                            //checking if a new optimum has been found
                            if(candidate.getCost()<newOptimum)
                            {
                                previousOptimum = newOptimum;
                                newOptimum=candidate.getCost();
                                bestSolution = candidate;
                                individualPerc+=(100-(newOptimum*100/previousOptimum));
                                populationPerc+=(100-(newOptimum*100/previousOptimum));
                                individualChance = solutions.size()*individualPerc/100;
                            }

                            if(sizeControl==1 && definitiveChildren.size()>populationSizeTruncation)
                            {
                                definitiveChildren = new ArrayList<>(definitiveChildren.parallelStream()
                                        .sorted()
                                        .collect(Collectors.toList()).subList(0,populationSizeTruncation));
                                System.gc();
                            }
                    }

                    if(individualChance<=0)
                        break;
                }
                solutions = new ArrayList<>(solutions.subList(j,solutions.size()));
                realJ+=j;
                j=0;

                Map<Float, Solution> map2 = new LinkedHashMap<>();
                for (Solution s : definitiveChildren) {
                    map.put(Float.parseFloat(new String(String.valueOf((int)s.getCost()))), s);
                }
                shift = definitiveChildren.size()-map.size();
                definitiveChildren.clear();
                definitiveChildren.addAll(map.values());
                map2.clear();
            }

            //if we obtained no offspring or optimum at all
            if(earlyStop==1 && newOptimum==optimum && i>0 || definitiveChildren.size()==0)
            {
                System.out.println("No new optimum or no offsprings");
                try {
                    saveFile = new PrintWriter(new FileOutputStream(new File(saveFilePath), true));
                } catch (FileNotFoundException e) {
                    System.out.println("Couldn't find or create the savefile");
                    return;
                }
                saveFile.append("\nNo new optimum or no offsprings");
                saveFile.close();

                if(newOptimum<optimum)
                    optimum=newOptimum;

                break;
            }

            if(newOptimum<optimum)
                optimum=newOptimum;

            solutions.addAll(definitiveChildren);
            definitiveChildren.clear();

            //re-sorting the population
            if(sizeControl==1 && solutions.size()>populationSizeTruncation)
            {
                solutions = new ArrayList<>(solutions.parallelStream()
                        .sorted()
                        .collect(Collectors.toList()).subList(0,populationSizeTruncation));
                System.gc();
            }
            else
            {
                solutions = new ArrayList<>(solutions.parallelStream()
                        .sorted()
                        .collect(Collectors.toList()));
                System.gc();
            }


            if(elitarianism.get(1)==0)
                solutions = eliteRemover(solutions, splitBy.get(1), splitPerc.get(1));


            //updating the tolerance
            if(tolerance-reduction>0)
                tolerance-=reduction;

            stopTime=System.currentTimeMillis();

            try {
                saveFile = new PrintWriter(new FileOutputStream(new File(saveFilePath),true));
            } catch (FileNotFoundException e) {
                System.out.println("Couldn't find or create the savefile");
                return;
            }


            //logging
            saveFile.append("\n-----------------------------------------------------");
            saveFile.append("\nEnded iteration "+i+"/"+(iterations-1)+" after "+(stopTime-startTime)/1000+" seconds"+
                    "\nObtained a new population of "+solutions.size()+
                    "\nReached the best optimum of "+newOptimum+
                    "\nAt the end the population chance was ~"+(int)((populationChance*100)/solutions.size())+"%");

            saveFile.append("\nBest solution in this iteration: ");
            saveFile.append("\n");
            for(Route r: bestSolution.getRoutes())
            {
                int deliverying = 0;
                int pickupping = 0;
                for(Node n: r.getNodes())
                {
                    saveFile.append("\t-->"+n.getId());
                    if(n.getDelivery()>0 && n.getId()!=0)
                    {
                        saveFile.append("\t-->LIN");
                        deliverying+=n.getDelivery();
                    }
                    else if(n.getId()!=0)
                    {
                        saveFile.append("\t-->BACK");
                        pickupping+=n.getPickup();
                    }
                    else
                        saveFile.append("\t-->DEPOT");

                    saveFile.append("\n");
                }
                saveFile.append("");
                saveFile.append("\n");
                saveFile.append("Total delivery: "+deliverying+"<"+datas.getCapacity_per_vechicle()+" / Total pickup: "+pickupping+"<"+datas.getCapacity_per_vechicle());
                saveFile.append("\n");
            }
            saveFile.close();

        }

        //final sort and logging
        executor.shutdownNow();

        solutions = new ArrayList<>(solutions.parallelStream()
                .sorted()
                .collect(Collectors.toList()).subList(0,solutions.size()));

        optimum = solutions.get(0).getCost();

        try {
            saveFile = new PrintWriter(new FileOutputStream(new File(saveFilePath),true));
        } catch (FileNotFoundException e) {
            System.out.println("Couldn't find or create the savefile");
            return;
        }

        saveFile.println("\n\n"+solutions.size()+" solutions globally");
        saveFile.println("Best solution: "+bestSolution.getCost());
        saveFile.println();
        saveFile.append("\nBest solution in this iteration: ");
        saveFile.append("\n");
        for(Route r: bestSolution.getRoutes())
        {
            int deliverying = 0;
            int pickupping = 0;
            for(Node n: r.getNodes())
            {
                saveFile.append("\t-->"+n.getId());
                if(n.getDelivery()>0 && n.getId()!=0)
                {
                    saveFile.append("\t-->LIN");
                    deliverying+=n.getDelivery();
                }
                else if(n.getId()!=0)
                {
                    saveFile.append("\t-->BACK");
                    pickupping+=n.getPickup();
                }
                else
                    saveFile.append("\t-->DEPOT");

                saveFile.append("\n");
            }
            saveFile.append("");
            saveFile.append("\n");
            saveFile.append("Total delivery: "+deliverying+"<"+datas.getCapacity_per_vechicle()+" / Total pickup: "+pickupping+"<"+datas.getCapacity_per_vechicle());
            saveFile.append("\n");
        }
        saveFile.close();

        File oldName = new File(saveFilePath);
        File newName = new File(newPartialName+" "+bestSolution.getCost()+" truncation: "+populationSizeTruncation);
        oldName.renameTo(newName);
        System.out.println("\n\n\n\n");
        solutions.clear();
        definitiveChildren.clear();
    }

    public static ArrayList<Solution> eliteRemover(ArrayList<Solution> solutions, int splitBy, int splitPerc)
    {
        ArrayList<ArrayList<Solution>> temp = new ArrayList<>();
        int sectorDimension = solutions.size()/splitBy;
        int index = 1;
        int firstEl = 0;

        while(index<=splitBy)
        {
            temp.add(new ArrayList<>());
            temp.get(temp.size()-1).addAll(solutions.subList(firstEl,(index)*sectorDimension));
            firstEl = (index)*sectorDimension;
            index++;
        }

        for(int u=0; u<splitBy; u++)
        {
            int currentSize = temp.get(u).size();
            ArrayList<Solution> toAdd = new ArrayList<>();
            toAdd.addAll(temp.get(u).subList(0,(currentSize*splitPerc/100)));
            temp.set(u,toAdd);
        }

        solutions.clear();
        for(ArrayList<Solution> portion : temp)
            solutions.addAll(portion);

        return solutions;

    }
}
