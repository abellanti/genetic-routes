import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.Callable;

/*
 * This is the proposed crossover operator that mates two solutions in order to generate two offsprings
 *
 * The algorithm is the following:
 *   1) The n routes that composes one solution (with n being the number of vehicles), are concatenated; the same procedure
 *      is repeated for the second solution. This allows us to have two perfectly symmetric solution vectors, and that
 *      both has all the nodes (delivery and pickups) only one time. the initial depot is removed from both of them for
 *      simplicity
 *
 *   2) then every vector is considered as a directed graph. I.E the solution (0) -> (1) -> (2) -> (3) -> (0) is considered as
 *      the graph with the nodes 1,2,3, each one linked with the previous one with a directed arch (obviously  every
 *      subgraph starts and ends in a depot)
 *
 *   3) to obtain an offspring (let's say, offspring1), we consider the first node of the first parent and of the second
 *      parent, and put one of them, along with it's destination inside the child. Wether we should get the second node
 *      from one parent or another is decided by the arch cost (Euclidian Distance)
 *
 *      example:
 *      (0)->(1)->(2)->(3)->(0)
 *      (0)->(2)->(1)->(3)->(0)
 *
 *      the child will start from (0) (this is always true) and then have (1)->(2) or (1)->(3) as the first arch, depending on the cost of
 *      each one of them. The cheaper arch shall be chosen.
 *
 *      A couple of details must be explained: if one of the two arch ends up in the depot then that arch shall be
 *      chosen, so the "ending" archs got priority. If the two archs are equal the first one shall be chosen at priori,
 *      but the nodes shall be removed from each vector. Beside these two cases, after the arch choice the second node
 *      is removed from every vector
 *
 *      Due to the randomic nature of the operator there could be some problems with the generations: for example we
 *      know that we have to delivery first, then we can pickup. This is not controlled during the initial offspring
 *      phase, so in order to fix it we check every node of the child and get which ones are delivery or pickup nodes.
 *      Then we re-insert the obtained nodes so that we first deliver, then pickup.
 *
 *      We could also obtain child that are just too heavy to be served (delivery/pickup that exceeds vehicle's capacity)
 *      This is fixed by a round robin re-assignement logic: every route that is too heavy loses nodes until it is legal,
 *      then for every node the first Route (in RR logic) that can carry it is chosen.
 *
 *      Every crossover is applied two times, by swapping the parents order, and both are kept, in order to avoid elitarism.
 *
 *      There's also a "reorder" phase after the crossover that every valid offspring shall receive: its nodes are
 *      re-ordered so that every node goes to it's nearest neighbour node.
 *
 * */
public class Crossover implements Callable<ArrayList<Solution>>{
    ArrayList<ArrayList<Route>> solutions; //the main solution that we will mate
    private ArrayList<Route> mainSolution; //all the other solutions to mate with
    private int capacity; //vehicle capacity
    private float optimum; //the best optimum so far
    private int tolerance; //tolerance for offspring acceptance
    private int limit; //limit at which we truncate and sort for RAM saving purposes




    public Crossover(ArrayList<Route> mainSolution, List<Solution> solutions, int capacity, float optimum, int tolerance, int limit)
    {
        this.solutions=new ArrayList<>();
        if(solutions!=null)
        {
            for(Solution s: solutions)
                this.solutions.add(s.getRoutes());
        }
        setMainSolution(mainSolution);
        setCapacity(capacity);
        this.optimum=optimum;
        this.tolerance=tolerance;
        this.limit=limit;
    }

    public ArrayList<Route> likeMonkeys(ArrayList<Route> first, ArrayList<Route> second)
    {

        //the symmetric vectors
        ArrayList<Node> parent1 = new ArrayList<>();
        ArrayList<Node> parent2 = new ArrayList<>();


        //appending the two solutions inside the vectors, excluding the first depot
        for (Route r : first) {
            parent1.addAll(r.getNodes().subList(1, r.getNodes().size()));
        }

        for (Route r : second) {
            parent2.addAll(r.getNodes().subList(1, r.getNodes().size()));

        }


        ArrayList<Route> child = new ArrayList<>(); //the offspring

        //adding the depot as the first node in every route
        for (int i = 0; i < first.size(); i++)
            child.add(new Route(first.get(0).getNodes().get(0)));


        int numberOfValidRoutes = 0;
        int index = 0; //tells us where the first node of the arch is in the first vector
        int indexinSecondSolution = -1; //...and where to find it in the second vector
        int currentRouteIndex = 0;
        Route tempRoute = new Route();
        Route tempRoute2 = new Route();
        short finished = 0; //it obviously tells us when we have finished


        //repeat until all the routes in the child are valid, a.k.a until we have n_routes = n_vehicles
        while (numberOfValidRoutes != first.size())
        {
            finished = 0;

            while (finished==0)
            {
                indexinSecondSolution = -1;
                finished = 0; //oh you...

                //here we will store two temporary routes in order to confront the cost of two archs
                tempRoute = new Route();
                tempRoute2 = new Route();

                //adding two nodes to every route
                //if we are at the end of the first vector we simply start again from the beginning
                if (index > parent1.size() - 1)
                    index = 0;

                /* We can't accept archs that starts from the depot, so we just shift to the next node until a valid one
                 * is found. The case in which the vector contains the depot only is prevented by the fact that the first
                 * and last one depot are removed, and we also remove the depots that are terminal nodes of an arch*/
                while (parent1.get(index).getId() == 0)
                    index = (index + 1) % parent1.size();

                //adding the first node of the arch
                tempRoute.appendNode(parent1.get(index));

                //If I'm over the end of the parent with the offset I go to the depot
                if (index + 1 > parent1.size() - 1)
                    tempRoute.appendNode(first.get(0).getNodes().get(0));
                else
                    tempRoute.appendNode(parent1.get(index + 1));

                //looking for the same node, but in the other parent
                for (Node n : parent2)
                {
                    indexinSecondSolution++;
                    if (n.getId() == tempRoute.getNodes().get(0).getId())
                        break;
                }

                //adding the same first node, but to the second arch
                tempRoute2.appendNode(parent2.get(indexinSecondSolution));

                short end = 0;
                //If I'm over the end of the parent with the offset I go to the depot
                if (indexinSecondSolution + 1 > parent2.size() - 1)
                {
                    tempRoute2.appendNode(first.get(0).getNodes().get(0));
                    end = 1;
                }
                else
                    tempRoute2.appendNode(parent2.get(indexinSecondSolution + 1));

                //getting the cost of each arch
                tempRoute.calculateCost();
                tempRoute2.calculateCost();


                //constructing the section of the child with the cheaper route

                //case 1: the first arch is cheaper and the second one doesn't terminate in a depot (or if the first arch itself terminates in a depot)
                if (tempRoute.getNodes().get(1).getId() == 0 || (tempRoute.getCost() <= tempRoute2.getCost() && tempRoute2.getNodes().get(1).getId() != 0))
                {
                    //if the offset doesn't throw me outside of the vector and the second node is a depot I remove it from the vector
                    if (index + 1 <= (parent1.size()-1) && parent1.get(index+1).getId() == 0)
                    {
                        parent1.remove(index + 1);
                    }


                    //this is the first node, it shall be removed in every case
                    parent1.remove(index);

                    //now exploring the second arch, checking if they are of the same size
                    if (tempRoute.getNodes().size() == tempRoute2.getNodes().size())
                    {
                        //must handle the case in which the archs are the same
                        short sameRoutes = 1;
                        for (int i = 0; i < tempRoute.getNodes().size(); i++) {
                            if (tempRoute.getNodes().get(i).getId() != tempRoute2.getNodes().get(i).getId()) {
                                sameRoutes = 0;
                                break;
                            }
                        }
                        //if they are equals and the second node is the depot I remove the second node
                        if (sameRoutes==1 && end==0 && tempRoute.getNodes().get(1).getId() == 0)
                            parent2.remove(indexinSecondSolution + 1);
                    }
                    else
                    {
                        //the two routes has different sizes: here everything is lost, we will be trapped inside a dark world of pain and despair
                        MainExecutor.offspringsWithAsymmetricRoutes++;
                        return null;
                    }

                    //removing the first node from the second vector
                    parent2.remove(indexinSecondSolution);


                    //what does this even do? I just don't remember but don't remove it, or it might not work anymore
                    for (Node n : tempRoute.getNodes())
                    {
                        if (!child.get(currentRouteIndex).getNodes().contains(n) || n.getId() == 0)
                            child.get(currentRouteIndex).appendNode(n);
                    }
                }

                //parallel case but when the second route is cheaper
                else if (tempRoute2.getNodes().get(1).getId() == 0 || tempRoute2.getCost() <= tempRoute.getCost())
                {
                    if (end==0) {
                        if (parent2.get(indexinSecondSolution + 1).getId() == 0)
                            parent2.remove(indexinSecondSolution + 1);
                    }

                    parent1.remove(index);
                    parent2.remove(indexinSecondSolution);

                    for (Node n : tempRoute2.getNodes()) {
                        if (!child.get(currentRouteIndex).getNodes().contains(n) || n.getId() == 0)
                            child.get(currentRouteIndex).appendNode(n);

                    }

                    index = -1;
                    for (Node n : parent1) {
                        index++;
                        if (n.getId() == tempRoute2.getNodes().get(1).getId())
                            break;
                    }

                }

                if (child.get(currentRouteIndex).getNodes().size() >= 3 && child.get(currentRouteIndex).getNodes().get(child.get(currentRouteIndex).getNodes().size() - 1).getId() == 0)
                    finished = 1;
            }
            if (finished==1) {
                numberOfValidRoutes++;
                currentRouteIndex++;
            }

        }

        //if there are still nodes to visit every linehaul is added at the beginning of a route, every backhaul at the end (round robin logic)
        ArrayList<Node> temp = new ArrayList<>();

        //I don't need the depot, so I remove all of the remaining ones
        for(int i = 0; i < parent1.size(); i++)
        {
            if (parent1.get(i).getId() != 0)
                temp.add(parent1.get(i));
        }
        //the old swap trick
        parent1.clear();
        parent1.addAll(temp);
        temp.clear();

        index = 0;
        for (Node n: parent1)
        {
            if (n.getDelivery() > 0)
                child.get(index).addNode(1, n);
            else if (n.getPickup() > 0)
                child.get(index).addNode((child.get(index).getNodes().size() - 2), n);
            index = (index + 1) % child.size();
        }

        //these are the lin and back that makes the routes too heavy to be served, shall be used later
        ArrayList<Node> spareLineHauls = new ArrayList<>();
        ArrayList<Node> spareBackHauls = new ArrayList<>();


        //In this step the re-legalization of the routes is made. That is re-enabling the constraints that a Route serves all the linehauls first, and then all the backhauls
        //for every route of the generated child, its linehaul and backhauls are splitted, and re-added to get back the route in the correct order
        for (int i = 0; i < child.size(); i++)
        {
            int totalDelivery = 0;
            int totalPickup = 0;

            Route lineHaulNodes = new Route();
            Route backHaulNodes = new Route();
            //removing initial and final depot, to readd later
            child.get(i).removeNode(0);
            child.get(i).removeNode(child.get(i).getNodes().size() - 1);

            for (Node n : child.get(i).getNodes())
            {
                if (n.getDelivery() > 0)
                    lineHaulNodes.appendNode(n);
                else if(n.getPickup()>0)
                    backHaulNodes.appendNode(n);
            }

            //here we remove from the linehauls, and then from the beackhauls, the nodes that makes the route too heavy
            for(int j=0; j<lineHaulNodes.getNodes().size(); j++)
            {
                totalDelivery+=lineHaulNodes.getNodes().get(j).getDelivery();
                if(totalDelivery>getCapacity())
                {
                    spareLineHauls.add(lineHaulNodes.getNodes().get(j));
                    totalDelivery-=lineHaulNodes.getNodes().get(j).getDelivery();
                    lineHaulNodes.removeNode(j);
                    j--;
                }
            }

            for(int j=0; j<backHaulNodes.getNodes().size(); j++)
            {
                totalPickup+=backHaulNodes.getNodes().get(j).getPickup();
                if(totalPickup>getCapacity())
                {
                    spareBackHauls.add(backHaulNodes.getNodes().get(j));
                    totalPickup-=backHaulNodes.getNodes().get(j).getPickup();
                    backHaulNodes.removeNode(j);
                    j--;
                }
            }

            //re-ordering so that every route serves lin then back
            lineHaulNodes.appendNodes(backHaulNodes.getNodes());
            child.get(i).setNodes(lineHaulNodes.getNodes());

            //re-adding initial and final depot
            child.get(i).addNode(0,first.get(0).getNodes().get(0));
            child.get(i).addNode(child.get(i).getNodes().size(),first.get(0).getNodes().get(0));
        }

        //sort of the spare nodes
        spareLineHauls.sort(Comparator.naturalOrder());
        spareBackHauls.sort(Comparator.naturalOrder());


        //re-adding every spare nodes to the first route (RR) that can serve it without becoming too heavy
        for(int i=0; i<child.size(); i++)
        {
            ///TODO: what if the n node can't be served, cause it makes the route too heavy, but the n+1 or n+2...could be served?
            while(spareLineHauls.size()>0) //adding the highest possible number of nodes
            {
                //CASE 1: we can add more nodes
                if (child.get(i).getTotalDelivery() + spareLineHauls.get(0).getDelivery() <= getCapacity()) {
                    child.get(i).addNode(1, spareLineHauls.get(0));
                    spareLineHauls.remove(0);
                }
                //CASE 2: we can't
                else
                    break;
            }

            //same for backhauls
            while(spareBackHauls.size()>0)
            {
                if(child.get(i).getTotalPickup()+spareBackHauls.get(0).getPickup()<=getCapacity())
                {
                    child.get(i).addNode(child.get(i).getNodes().size()-2,spareBackHauls.get(0));
                    spareBackHauls.remove(0);
                }
                else
                    break;
            }
        }

        //in case there are still spare nodes then the offspring is illegal, since it doesn't serve all the nodes
        if(spareLineHauls.size()>0 || spareBackHauls.size()>0)
        {
            MainExecutor.offspringsThatDoesntServeAllTheNodes++;
            return null;
        }

        //checking if there are routes with backhauls only
        short linPresent = 0;
        for (Route r: child)
        {
            linPresent = 0;
            if(r.getNodes().size()<3)
                break;
            for (Node n : r.getNodes())
            {
                if (n.getDelivery() > 0)
                {
                    linPresent = 1;
                    break;
                }

            }
            if (linPresent==0)
            {
                MainExecutor.offspringsWithBackOnly++;
                return null; //again: pain and despair
            }
        }


        //if any of the route are too heavy to be served, then the child isn't legal, and null is returned instead
        for (Route r : child)
        {
            int totalDelivery = 0;
            int totalPickup = 0;
            for (Node n : r.getNodes())
            {
                totalDelivery += n.getDelivery();
                totalPickup += n.getPickup();
            }

            if (totalDelivery > getCapacity() || totalPickup > getCapacity())
            {
                MainExecutor.tooHeavyOffsprings++;
                return null;
            }

        }



        int numberOfRoutes = 0;

        //check of the legality of the routes
        for (Route r : child) {
            //a route can be legal if it has at least three nodes (literally depot->node->depot) and the last one must be a depot
            if (r.getNodes().size() < 3) {
                //System.out.println("Some routes are too short");
                MainExecutor.offspringsWithTooShortRoutes++;
                return null;
            }
            numberOfRoutes++;
        }

        //check on the number of routes
        if (numberOfRoutes < first.size())
        {
            MainExecutor.offspringsWithNotEnoughRoutes++;
            //System.out.println("Not enough routes");
            return null;
        }

        return child;

    }

    //main method that is called for multithreading purposes: every multithread operation that follows crossover should be here
    @Override
    public ArrayList<Solution> call()
    {
        //preventing a bad inizialization of the Crossover instance
        if (getSolutions() == null)
            return null;

        ArrayList<Solution> toReturn = new ArrayList<>(); //ArrayList to store all the valid offsprings
        ArrayList<ArrayList<Route>> offsprings = new ArrayList<>(); //general offsprings container

        //here the Crossover instance does all of its 'k' matings
        for (int k = 0; k < solutions.size(); k++)
        {

            //matings with swapped parents
            offsprings.add(likeMonkeys(mainSolution,solutions.get(k)));
            offsprings.add(likeMonkeys(solutions.get(k),mainSolution));

            for(int i =0; i<offsprings.size(); i++)
            {
                Solution sonAsSolution = null;
                if(offsprings.get(i)!=null)
                {
                    sonAsSolution  = new Solution(offsprings.get(i));


                    double originalCost = sonAsSolution.getCost();
                    Solution studied = school(sonAsSolution);
                    if(studied.getCost()<originalCost)
                    {
                        offsprings.set(i,studied.getRoutes());
                        MainExecutor.usefulSchool++;
                    }
                    else
                        MainExecutor.uselessSchool++;

                    if(sonAsSolution!= null && sonAsSolution.getCost()<=(optimum+((optimum*tolerance)/100)))
                        MainExecutor.belowTresholdOffsprings++;
                    else
                        MainExecutor.overThresholdOffsprings++;

                    sonAsSolution = new Solution(offsprings.get(i));
                }

                if(sonAsSolution!=null)
                    toReturn.add(sonAsSolution);
            }

            offsprings.clear(); //clearing the offspring container for the new iteration
        }

        if(limit!=0 && toReturn.size()>limit)
        {
            toReturn.sort(Comparator.naturalOrder());
            toReturn = new ArrayList<>(toReturn.subList(0,limit));
        }

        if(toReturn.size()>0)
        {
            reset();
            if(MainExecutor.elitarianism.get(2)==0)
                toReturn = MainExecutor.eliteRemover(toReturn, MainExecutor.splitBy.get(2), MainExecutor.splitPerc.get(2));
            return toReturn;
        }
        else
        {
            reset();
            System.gc();

            return null;
        }

    }



    public void setMainSolution(ArrayList<Route> mainSolution) {
        this.mainSolution = mainSolution;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public ArrayList<ArrayList<Route>> getSolutions() {
        return solutions;
    }

    public void reset()
    {
        this.solutions = null;
        this.mainSolution = null;
    }


    //Letting the offsprings "study", by sorting the linehauls and backhauls in the best order (based on cost)
     public static Solution school(Solution ignorant)
     {
        ArrayList<Route> routesToReorder = ignorant.getRoutes();
        ArrayList<Route> backup= new ArrayList<>(ignorant.getRoutes());
        double originalCost = ignorant.getCost();
        for(int i=0; i<routesToReorder.size(); i++)
        {
            Route temp = new Route(new ArrayList<>());
            ArrayList<Node> nodesToReorder = routesToReorder.get(i).getLineHauls();
            temp.appendNode(routesToReorder.get(i).getNodes().get(0)); //adding the depot

            int cont=0;

            for(int z=0; z<2; z++) //2 to reorder lin, then back
            {
                while(nodesToReorder.size()>0)
                {
                    double min = Double.MAX_VALUE;
                    int index = 0;
                    double dist;
                    //finding the closest node to the previous one
                    for (int j = 0; j < nodesToReorder.size(); j++)
                    {
                        dist = temp.getNodes().get(cont).getDistance(nodesToReorder.get(j));

                        if (dist < min)
                        {
                            index = j;
                            min = dist;
                        }

                    }
                    //adding the node
                    temp.appendNode(nodesToReorder.get(index));
                    nodesToReorder.remove(index);
                    cont++;
                }

                //again but with backhauls
                nodesToReorder=routesToReorder.get(i).getBackHauls();
            }
            //adding the final depot
            temp.appendNode(routesToReorder.get(i).getNodes().get(0));

            //accepting the ordered offspring only if it's improved
            if(temp.getCost()<backup.get(i).getCost())
                ignorant.overrideRoute(i,temp);
        }

        return new Solution(ignorant.getRoutes());
    }


}
