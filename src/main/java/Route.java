import java.util.ArrayList;

public class Route {

    private float cost;
    private ArrayList<Node> nodes;

    public Route(Node depot)
    {
        this.setCost(0.0f);
        this.nodes= new ArrayList<>();
        this.nodes.add(depot);
    }

    public Route(){
        this.nodes=new ArrayList<>();
        this.setCost(0.0f);

    }

    public Route(ArrayList<Node> nodes){
        this.nodes = nodes;
    }

    public int getLinCapacityLeft(int capacity)
    {
        Route temp = new Route(this.getLineHauls());
        int work = 0;
        for(Node n: temp.getNodes())
            work+=n.getActualWork();
        return capacity-work;
    }

    public int getLBackCapacityLeft(int capacity)
    {
        Route temp = new Route(this.getBackHauls());
        int work = 0;
        for(Node n: temp.getNodes())
            work+=n.getActualWork();
        return capacity-work;
    }


    public ArrayList<Node> getNodes() {
        return nodes;
    }

    public void setNodes(ArrayList<Node> nodes) {
        this.nodes = nodes;
    }

    public void appendNodes(ArrayList<Node> nodes) {
        this.nodes.addAll(nodes);
    }
    public void appendNode(Node node) {
        this.nodes.add(node);
    }



    public void addNodeOnTop(Node n)
    {
        this.nodes.add(0,n);
    }
    public void addNode(int index,Node n)
    {
        this.nodes.add(index,n);
    }


    //removes the specified node from the route (by index)
    public void removeNode(int index)
    {
        this.nodes.remove(index);
    }

    //calculates and sets the cost of this route, via euclidian distance
    public void calculateCost()
    {
        float result=0.0f;
        for(int i=1; i<nodes.size(); i++)
            result+= Math.sqrt(Math.pow(new Double(nodes.get(i-1).getX() - nodes.get(i).getX()), 2)
                    + Math.pow(new Double(nodes.get(i-1).getY() - nodes.get(i).getY()), 2));
        this.setCost(result);
    }

    public int getTotalPickup()
    {
        int toReturn = 0;
        for(Node n: getNodes())
            toReturn+=n.getPickup();
        return toReturn;
    }

    public int getTotalDelivery()
    {
        int toReturn = 0;
        for(Node n: getNodes())
            toReturn+=n.getDelivery();
        return toReturn;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(float cost) {
        this.cost = cost;
    }

    public ArrayList<Node> getLineHauls()
    {
        ArrayList<Node> toReturn = new ArrayList<>();
        for(Node n: this.getNodes())
        {
            if(n.getDelivery()>0 )
                toReturn.add(n);
        }

        return toReturn;
    }

    public ArrayList<Node> getBackHauls()
    {
        ArrayList<Node> toReturn = new ArrayList<>();
        for(Node n: this.getNodes())
        {
            if(n.getPickup()>0)
                toReturn.add(n);
        }

        return toReturn;
    }



}


