

/*        args[0] = data file (ex. A1.txt)
        * args[1] = save file (to write a brief review of the results)
        * args[2] = initial size of population
        * args[3] = number of iterations
        * args[4] = initial tolerance
        * args[5] = tolerance reduction per iteration
        * args[6] = individual mating chance %. In other words one individual can initially mate with only the args[6]% of the population, to prove
        *           that it is fertile (a.k.a.: can produce children that are lower than the optimum or than the optimum+the tolerance).
        *           For every child that beat the optimum (without the tolerance) this number is increased.
        *
        * args[7] = population mating chance %. Although args[6] speeds the iteration, every single one individual has its chances to mate, and
        *           if very large population are used this can lead to very slow iterations. Also this could lead to the forced choice
        *           to use very small args[6] values. So args[7] can put a stop to this. The population itself has a chance to demonstrate
        *           its fertility, in percentual. So the args[7]% of the total population can mate: every individual that get at least one
        *           child that beats the best optimum so far increases this percentual.
        *args[8] = population size control. If it is true then the population is refreshed with the children, but it can't grow from it's initial dimension*
        *args[9] = truncate population to this size in generation phase: this allows to generate a greater number of initial solutions but to use only args[11] of them.
        *           tipically args[2] that is way higher than this performs better
        *
        *args[10] = path for saving the best solution. So even if the algorithm is stopped it is possible to always retrieve the last best solution
        *args[11] = at which size a generic thread must truncate it's results
        *args[12] = population generation number per thread
        *args[13] = multiplicative factor per pool (generation)
        *args[14] = multiplicative factor per pool (crossover)
        *args[15] = multicore boolean switch -> just for testing purpose
        *args[16] = verbosity = if true the program prompt asking the user if he want to see the read data*/

import java.io.File;
import java.util.Arrays;

public class Benchmark {

    public Benchmark()
    {

    }

    public static void main(String[] args) throws InterruptedException {
        String populationSize = "1000000";
        String iterations = "200";
        String tolerance = "70";
        String reduction = "0";
        String individualChance = "100";
        String populationChance = "100";
        String sizeControl = "1";
        String populationSizeTruncation = "2000";
        String generationRamSaver = "0";
        String crossoverRamSaver = "0";
        String generationPerThread = "3000";
        String generationPoolFactor = "50";
        String crossoverPoolFactor = "2";
        String multithread = "1";
        File savePath = new File(args[0]);
        File instances = new File(args[1]);
        String verbosity = args[2];
        String splitBy = "4 10 1";
        String splitPerc = "25 60 100";
        String earlyStop = "0";
        String elitarianism = "0 0 0";

        File dir = new File(savePath.getAbsolutePath()+"/"+populationSize);
        dir.mkdir();
        File[] l = (instances.listFiles());
        Arrays.sort(l);

        for(File file: l)
        {
            String currentFile = file.getAbsolutePath();
            String[] arguments = new String[22];
            arguments[0]=(currentFile);
            arguments[1]=(dir.getAbsolutePath());
            arguments[2]=(populationSize);
            arguments[3]=(iterations);
            arguments[4]=(tolerance);
            arguments[5]=(reduction);
            arguments[6]=(individualChance);
            arguments[7]=(populationChance);
            arguments[8]=(sizeControl);
            arguments[9]=(populationSizeTruncation);
            arguments[10]=(dir.getAbsolutePath());
            arguments[11]=(generationRamSaver);
            arguments[12]=(crossoverRamSaver);
            arguments[13]=(generationPerThread);
            arguments[14]=(generationPoolFactor);
            arguments[15]=(crossoverPoolFactor);
            arguments[16]=(multithread);
            arguments[17]=(verbosity);
            arguments[18]=(splitBy);
            arguments[19]=(splitPerc);
            arguments[20]=(earlyStop);
            arguments[21]=elitarianism;

            System.out.println(arguments[0]);
            MainExecutor p = new MainExecutor();
            p.main(arguments);
            p = null;

            System.gc();
            Thread.sleep(1000);


        }
    }

}
